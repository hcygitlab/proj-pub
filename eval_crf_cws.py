from pyhanlp import JClass, SafeJClass
import msr
from crfpp_train_hanlp_load import CRF_MODEL_PATH, CRF_MODEL_TXT_PATH
from test_utility import get_eval_result


CRFSegmenter = JClass('com.hankcs.hanlp.model.crf.CRFSegmenter')
CRFLexicalAnalyzer = JClass('com.hankcs.hanlp.model.crf.CRFLexicalAnalyzer')
CWSEvaluator = SafeJClass('com.hankcs.hanlp.seg.common.CWSEvaluator')


def train(corpus):
    # segmenter = CRFSegmenter(None)
    # segmenter.train(corpus, CRF_MODEL_PATH)
    # return CRFLexicalAnalyzer(segmenter)
    # 訓練完畢後，可傳入txt格式的模型（不可傳入CRF++的二進制模型，不相容）
    return CRFLexicalAnalyzer(CRF_MODEL_TXT_PATH).enableCustomDictionary(False)


if __name__ == '__main__':
    segment = train(msr.msr_train)
    result = CWSEvaluator.evaluate(segment, msr.msr_test, msr.msr_output, msr.msr_gold, msr.msr_dict)
    eval_crf_cws_result = get_eval_result(result)
