from test_utility import ensure_data
import re
import os
from pyhanlp import JClass

def to_region(segmentation: str) -> list:
    '''
    分詞結果轉換為區間 tuple 的 list
    '''
    region = []
    start = 0

    for word in re.compile('\\s+').split(segmentation.strip()):
        end = start + len(word)
        region.append((start, end))
        start = end

    return region

def prf(gold: str, pred: str, dic) -> tuple:
    '''
    gold: 答案
    pred: 分詞結果
    return (P, R, F1, OOV_R, IV_R)
    '''
    A_size, B_size, A_cap_B_size, OOV, IV, OOV_R, IV_R = 0, 0, 0, 0, 0, 0, 0

    with open(gold, encoding='utf-8') as gd, open(pred, encoding='utf-8') as pd:
        for g, p in zip(gd, pd):
            A, B = set(to_region(g)), set(to_region(p))
            A_size += len(A)
            B_size += len(B)
            A_cap_B_size += len(A & B)
            text = re.sub('\\s+', '', g)
            for (start, end) in A:
                word = text[start: end]
                if dic.containsKey(word):
                    IV += 1
                else:
                    OOV += 1

            for (start, end) in A & B:
                word = text[start: end]
                if dic.containsKey(word):
                    IV_R += 1
                else:
                    OOV_R += 1

    p, r = A_cap_B_size / B_size * 100, A_cap_B_size / A_size * 100

    return p, r, 2 * p * r / (p + r), OOV_R / OOV * 100, IV_R / IV * 100

if __name__ == '__main__':
    corpus = 'msr'
    sighan05 = ensure_data('icwb2-data', 'http://sighan.cs.uchicago.edu/bakeoff2005/data/icwb2-data.zip')
    msr_dict = os.path.join(sighan05, 'gold', f'{corpus}_training_words.utf8')
    msr_test = os.path.join(sighan05, 'testing', f'{corpus}_testing.utf8')
    msr_output = os.path.join(sighan05, 'testing', f'{corpus}_output.txt')
    msr_gold = os.path.join(sighan05, 'gold', f'{corpus}_test_gold.utf8')

    DoubleArrayTrieSegment = JClass('com.hankcs.hanlp.seg.Other.DoubleArrayTrieSegment')
    segment = DoubleArrayTrieSegment([msr_dict]).enablePartOfSpeechTagging(True)

    with open(msr_gold, encoding='utf-8') as test, open(msr_output, 'w', encoding='utf-8') as output:
        for line in test:
            output.write('  '.join(term.word for term in segment.seg(re.sub("\\s+", "", line))))
            output.write('\n')


    eval_cws_result = 'P:{:.2f} R:{:.2f} OOV-R:{:.2f} IV-R:{:.2f}'.format(*prf(msr_gold, msr_output, segment.trie))
