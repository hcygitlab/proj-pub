import os
from collections import Counter
import numpy as np
from test_utility import ensure_data


sighan05 = ensure_data('icwb2-data', 'http://sighan.cs.uchicago.edu/bakeoff2005/data/icwb2-data.zip')
msr = os.path.join(sighan05, 'training', 'msr_training.utf8')

f = Counter()
with open(msr, encoding='utf-8') as src:
    for line in src:
        line = line.strip()
        for word in line.split('  '):
            # word = word.strip()
            # if len(word) < 2: continue
            f[word] += 1


def plot(token_counts, title='Zipf\'s Law', ylabel='Word Frequency'):
    from matplotlib import pyplot as plt
    # plt.rcParams['axes.unicode_minus'] = False

    # figure = {
    #     'figsize': (20, 10),
    #     'dpi': 144
    # }
    # plt.rc('figure', **figure)
    font = {
        'family' : 'Heiti TC',
        'weight' : 'normal',
        'size'   : 16
    }
    fig = plt.figure(
        figsize=(20, 6),
        dpi=144
    )
    plt.rc('font', **font)
    ax = fig.add_subplot(111)
    token_counts = list(zip(*token_counts))
    num_elements = np.arange(len(token_counts[0]))
    top_offset = max(token_counts[1]) + len(str(max(token_counts[1])))
    ax.set_title(title)
    ax.set_xlabel('Rank')
    ax.set_ylabel(ylabel)
    ax.xaxis.set_label_coords(1.05, 0.015)
    ax.set_xticks(num_elements)
    ax.set_xticklabels(token_counts[0], rotation=55, verticalalignment='top')
    ax.set_ylim([0, top_offset])
    ax.set_xlim([-1, len(token_counts[0])])
    rects = ax.plot(num_elements, token_counts[1], linewidth=1.5)
    plt.show()


if __name__ == '__main__':
    word_freq = f.most_common(50)
    plot(word_freq)

