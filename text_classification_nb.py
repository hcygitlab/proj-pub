import os
from pyhanlp import SafeJClass, IOUtil, HanLP
from test_utility import ensure_data
import pandas as pd

NaiveBayesClassifier = SafeJClass('com.hankcs.hanlp.classification.classifiers.NaiveBayesClassifier')
IOUtil = SafeJClass('com.hankcs.hanlp.corpus.io.IOUtil')
sogou_corpus_path = ensure_data('搜狗文本分类语料库迷你版',
                                'http://file.hankcs.com/corpus/sogou-text-classification-corpus-mini.zip')


def train_or_load_classifier():
    model_path = sogou_corpus_path + '.ser'
    if os.path.isfile(model_path):
        return NaiveBayesClassifier(IOUtil.readObjectFrom(model_path))
    classifier = NaiveBayesClassifier()
    classifier.train(sogou_corpus_path)
    model = classifier.getModel()
    IOUtil.saveObjectTo(model, model_path)
    return NaiveBayesClassifier(model)


def predict(classifier, text):
    return [HanLP.s2tw(text), HanLP.s2tw(classifier.classify(text))]


if __name__ == '__main__':
    classifier = train_or_load_classifier()
    sentences = [
        'C罗获2018环球足球奖最佳球员 德尚荣膺最佳教练',
        '研究生考录模式亟待进一步专业化',
        '如果真想用食物解压,建议可以食用燕麦',
        '通用及其部分竞争对手目前正在考虑解决库存问题'
        ]
    text_classification_nb_result = []
    for sentence in sentences:
        result = predict(classifier, sentence)
        text_classification_nb_result.append(result)
    text_classification_nb_result = pd.DataFrame(data=text_classification_nb_result, columns=['文章', '分類'])
