from utils import MyDb

def main():
    db = MyDb()
    try:
        db.cur.execute('select count(*),max(length(content)) from records;')
        link_count,maxlength = db.cur.fetchone()
        db.cur.execute("select pg_size_pretty(pg_database_size('nlp'));")
        size, = db.cur.fetchone()
        print(f'records: count={link_count}, maxlength={maxlength}, size={size}')
    except Exception as e:
        raise e
    finally:
        db.close()

if __name__ == '__main__':
    main()
