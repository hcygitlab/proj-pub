-- Table: comments
-- DROP TABLE comments;
CREATE TABLE comments
(
    cid serial,
    aid integer,
    bid integer,
    userid text,
    tag text,
    comment text,
    datetime text,
    remark text,
    CONSTRAINT comment_key PRIMARY KEY (cid),
    CONSTRAINT comments_aid_fkey FOREIGN KEY (aid)
        REFERENCES articles (aid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT comments_bid_fkey FOREIGN KEY (bid)
        REFERENCES boards (bid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

