-- Table: categories
-- DROP TABLE categories;
CREATE TABLE categories
(
    code smallint,
    name text
);

-- Table: boards
-- DROP TABLE boards;
CREATE TABLE boards
(
    bid serial,
    name text,
    url text,
    adult boolean,
    CONSTRAINT board_key PRIMARY KEY (bid)
);

-- Table: articles
-- DROP TABLE articles;
CREATE TABLE articles
(
    aid serial,
    bid integer,
    author text,
    title text,
    article text,
    category smallint,
    post_time timestamp without time zone,
    remark text,
    CONSTRAINT article_key PRIMARY KEY (aid),
    CONSTRAINT articles_bid_fkey FOREIGN KEY (bid)
        REFERENCES boards (bid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- Table: comments
-- DROP TABLE comments;
CREATE TABLE comments
(
    cid serial,
    aid integer,
    bid integer,
    userid text,
    tag text,
    comment text,
    datetime text,
    remark text,
    CONSTRAINT comment_key PRIMARY KEY (cid),
    CONSTRAINT comments_aid_fkey FOREIGN KEY (aid)
        REFERENCES articles (aid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT comments_bid_fkey FOREIGN KEY (bid)
        REFERENCES boards (bid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

insert into categories (code, name) values (0, 'Politics');
insert into boards (name, url, adult) values ('Gossiping', 'https://www.ptt.cc/bbs/Gossiping/index.html', true);
