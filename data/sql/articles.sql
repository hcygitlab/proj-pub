-- Table: articles
-- DROP TABLE articles;
CREATE TABLE articles
(
    aid serial,
    bid integer,
    author text,
    title text,
    article text,
    category smallint,
    post_time timestamp without time zone,
    remark text,
    CONSTRAINT article_key PRIMARY KEY (aid),
    CONSTRAINT articles_bid_fkey FOREIGN KEY (bid)
        REFERENCES boards (bid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
