-- Table: records
-- DROP TABLE records;
CREATE TABLE records
(
    lid serial,
    link text,
    title text,
    content text
);

