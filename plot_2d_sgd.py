import matplotlib.pyplot as plt
import numpy as np
import matplotlib.lines as mlines


plt.rcParams['font.sans-serif'] = ['Heiti TC']
plt.rcParams['axes.unicode_minus'] = False
figure = {
    'figsize': (6, 5),
    'dpi': 144
}
plt.rc('figure', **figure)


def newline(p1, p2, color=None, marker=None):
    """
    https://stackoverflow.com/questions/36470343/how-to-draw-a-line-with-matplotlib
    :param p1:
    :param p2:
    :return:
    """
    ax = plt.gca()
    xmin, xmax = ax.get_xbound()

    if (p2[0] == p1[0]):
        xmin = xmax = p1[0]
        ymin, ymax = ax.get_ybound()
    else:
        ymax = p1[1] + (p2[1] - p1[1]) / (p2[0] - p1[0]) * (xmax - p1[0])
        ymin = p1[1] + (p2[1] - p1[1]) / (p2[0] - p1[0]) * (xmin - p1[0])

    l = mlines.Line2D([xmin, xmax], [ymin, ymax], color=color, marker=marker)
    ax.add_line(l)
    return l


x = np.linspace(-1.5, 1.5)
y = x ** 2

newline([1, 0], [1, 1], color='g')

plt.ylim([0, 2])

plt.plot(x, y)
plt.title('$J(w)=w^2$')
plt.xlabel('$w$')
plt.ylabel('$J(w)$')
plt.annotate('梯度$\Delta w = 2$', xy=(1, 1), xytext=(0, 1.0), ha='center',
             arrowprops=dict(facecolor='black', shrink=0.05),
             )

bbox_props = dict(boxstyle="larrow", fc='w', ec="black", lw=2)
t = plt.text(0.6, 0.1, '下降方向', ha='center', va='center', rotation=0,
             bbox=bbox_props)
bbox_props['boxstyle'] = 'rarrow'
plt.text(1.4, 0.1, "上升方向", ha='center', va='center', rotation=0,
         bbox=bbox_props)

plt.show()
