from pyhanlp import HanLP, IOUtil
from test_utility import ensure_data
import os


HLM_PATH = ensure_data('红楼梦.txt', 'http://file.hankcs.com/corpus/红楼梦.zip')
XYJ_PATH = ensure_data('西游记.txt', 'http://file.hankcs.com/corpus/西游记.zip')
SHZ_PATH = ensure_data('水浒传.txt', 'http://file.hankcs.com/corpus/水浒传.zip')
SAN_PATH = ensure_data('三国演义.txt', 'http://file.hankcs.com/corpus/三国演义.zip')
WEIBO_PATH = ensure_data('weibo-classification', 'http://file.hankcs.com/corpus/weibo-classification.zip')


def test_weibo():
    for folder in os.listdir(WEIBO_PATH):
        print(folder)
        big_text = ''
        for file in os.listdir(os.path.join(WEIBO_PATH, folder)):
            with open(os.path.join(WEIBO_PATH, folder, file), encoding='utf-8') as src:
                big_text += ''.join(src.readlines())
        word_info_list = HanLP.extractWords(big_text, 100)
        print(word_info_list)


def extract(corpus, new=False):
    word_info_list = HanLP.extractWords(IOUtil.newBufferedReader(corpus), 100, True) if new is True else HanLP.extractWords(IOUtil.newBufferedReader(corpus), 100)
    return word_info_list


def get_tc_array(arr):
    return [HanLP.s2tw(str(word)) for word in arr]


if __name__ == '__main__':
    extract_words_result_hlm = get_tc_array(extract(HLM_PATH))
    extract_words_result_hlm_new = get_tc_array(extract(HLM_PATH, new=True))
    extract_words_result_xyj = get_tc_array(extract(XYJ_PATH))
    extract_words_result_xyj_new = get_tc_array(extract(XYJ_PATH, new=True))
    extract_words_result_shZ = get_tc_array(extract(SHZ_PATH))
    extract_words_result_shZ_new = get_tc_array(extract(SHZ_PATH, new=True))
    extract_words_result_san = get_tc_array(extract(SAN_PATH))
    extract_words_result_san_new = get_tc_array(extract(SAN_PATH, new=True))

