from pyhanlp import JClass
from text_classification_nb import sogou_corpus_path

# IClassifier = JClass('com.hankcs.hanlp.classification.classifiers.IClassifier')
NaiveBayesClassifier = JClass('com.hankcs.hanlp.classification.classifiers.NaiveBayesClassifier')
LinearSVMClassifier = JClass('com.hankcs.hanlp.classification.classifiers.LinearSVMClassifier')
FileDataSet = JClass('com.hankcs.hanlp.classification.corpus.FileDataSet')
# IDataSet = JClass('com.hankcs.hanlp.classification.corpus.IDataSet')
MemoryDataSet = JClass('com.hankcs.hanlp.classification.corpus.MemoryDataSet')
Evaluator = JClass('com.hankcs.hanlp.classification.statistics.evaluations.Evaluator')
# FMeasure = JClass('com.hankcs.hanlp.classification.statistics.evaluations.FMeasure')
# BigramTokenizer = JClass('com.hankcs.hanlp.classification.tokenizers.BigramTokenizer')
HanLPTokenizer = JClass('com.hankcs.hanlp.classification.tokenizers.HanLPTokenizer')
ITokenizer = JClass('com.hankcs.hanlp.classification.tokenizers.ITokenizer')


def evaluate(classifier, tokenizer):
    training_corpus = FileDataSet().setTokenizer(tokenizer).load(sogou_corpus_path, 'UTF-8', 0.9)
    classifier.train(training_corpus)
    testing_corpus = MemoryDataSet(classifier.getModel()).load(sogou_corpus_path, 'UTF-8', -0.1)
    result = Evaluator.evaluate(classifier, testing_corpus)
    # print(classifier.getClass().getSimpleName() + '+' + tokenizer.getClass().getSimpleName())
    # print(result)
    return result


if __name__ == '__main__':
    nb_token_result = evaluate(NaiveBayesClassifier(), HanLPTokenizer())
    # nb_bigram_result = evaluate(NaiveBayesClassifier(), BigramTokenizer())
    svm_token_result = evaluate(LinearSVMClassifier(), HanLPTokenizer())
    # svm_bigram_result = evaluate(LinearSVMClassifier(), BigramTokenizer())
