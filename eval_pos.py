from pyhanlp import JClass
from test_utility import ensure_data
from pku import PKU199801_TRAIN, PKU199801_TEST, POS_MODEL


PosTagUtil = JClass('com.hankcs.hanlp.dependency.nnparser.util.PosTagUtil')
FirstOrderHiddenMarkovModel = JClass('com.hankcs.hanlp.model.hmm.FirstOrderHiddenMarkovModel')
SecondOrderHiddenMarkovModel = JClass('com.hankcs.hanlp.model.hmm.SecondOrderHiddenMarkovModel')
AbstractLexicalAnalyzer = JClass('com.hankcs.hanlp.tokenizer.lexical.AbstractLexicalAnalyzer')
PerceptronSegmenter = JClass('com.hankcs.hanlp.model.perceptron.PerceptronSegmenter')
PerceptronPOSTagger = JClass('com.hankcs.hanlp.model.perceptron.PerceptronPOSTagger')


def train_hmm_pos(corpus, model):
    HMMPOSTagger = JClass('com.hankcs.hanlp.model.hmm.HMMPOSTagger')
    tagger = HMMPOSTagger(model)  # 創建詞性標註器
    tagger.train(corpus)  # train

    # print(', '.join(tagger.tag('他', '的', '希望', '是', '希望', '上学')))  # predict
    analyzer = AbstractLexicalAnalyzer(PerceptronSegmenter(), tagger)  # 構造詞性語法分析器
    # print(analyzer.analyze('他的希望是希望上学'))  # 分詞 + 詞性標註
    return tagger


def train_crf_pos(corpus):
    CRFPOSTagger = JClass('com.hankcs.hanlp.model.crf.CRFPOSTagger')

    # 選項1. 使用HanLP的Java API訓練，慢
    # tagger = CRFPOSTagger(None)  # 創建空白標註器
    # tagger.train(corpus, POS_MODEL)  # train
    # tagger = CRFPOSTagger(POS_MODEL) # load

    # 選項2. 使用CRF++訓練，HanLP加載 (訓練命令由選項1給出)
    tagger = CRFPOSTagger(POS_MODEL + '.txt')

    # print(', '.join(tagger.tag('他', '的', '希望', '是', '希望', '上学')))  # predict
    analyzer = AbstractLexicalAnalyzer(PerceptronSegmenter(), tagger)  # 構造詞性語法分析器
    # print(analyzer.analyze('他的希望是希望上学'))  # 分詞 + 詞性標註
    return tagger


def train_perceptron_pos(corpus):
    # POSTrainer = JClass('com.hankcs.hanlp.model.perceptron.POSTrainer')
    # trainer = POSTrainer()
    # trainer.train(corpus, POS_MODEL)  # train

    tagger = PerceptronPOSTagger(POS_MODEL)  # load

    # print(', '.join(tagger.tag('他', '的', '希望', '是', '希望', '上学')))  # predict
    analyzer = AbstractLexicalAnalyzer(PerceptronSegmenter(), tagger)  # 構造詞性語法分析器
    # print(analyzer.analyze('他的希望是希望上学'))  # 分詞 + 詞性標註
    return tagger


if __name__ == '__main__':
    def evaluate(result, corpus):
        text = '{:.2f}'.format(PosTagUtil.evaluate(result, corpus))
        return text

    eval_pos_first_order_result = evaluate(train_hmm_pos(PKU199801_TRAIN, FirstOrderHiddenMarkovModel()), PKU199801_TEST)
    eval_pos_second_order_result = evaluate(train_hmm_pos(PKU199801_TRAIN, SecondOrderHiddenMarkovModel()), PKU199801_TEST)
    eval_pos_perceptron_result = evaluate(train_perceptron_pos(PKU199801_TRAIN), PKU199801_TEST)
    eval_pos_crf_result = evaluate(train_crf_pos(PKU199801_TRAIN), PKU199801_TEST)

    # print('一階HMM: {}%'.format(eval_pos_first_order_result))
    # print('二階HMM: {}%'.format(eval_pos_second_order_result))
    # print('感知機: {}%'.format(eval_pos_perceptron_result))
    # print('CRF: {}%'.format(eval_pos_crf_result))

