import zipfile
import os
from pyhanlp import IOUtil, HanLP, JClass
from pyhanlp.static import download, remove_file #, HANLP_DATA_PATH

tw2s, s2tw = HanLP.tw2s, HanLP.s2tw

IClassifier = JClass('com.hankcs.hanlp.classification.classifiers.IClassifier')
NaiveBayesClassifier = JClass('com.hankcs.hanlp.classification.classifiers.NaiveBayesClassifier')
LinearSVMClassifier = JClass('com.hankcs.hanlp.classification.classifiers.LinearSVMClassifier')


def test_data_path():
    # data_path = os.path.join(HANLP_DATA_PATH, 'test')
    # data_path = os.path.abspath(os.path.join(os.pardir, 'data/test'))
    data_path = os.path.join(os.getcwd(), 'data/test')
    if not os.path.isdir(data_path):
        os.mkdir(data_path)
    return data_path


def ensure_data(data_name, data_url):
    root_path = test_data_path()
    dest_path = os.path.join(root_path, data_name)
    if os.path.exists(dest_path):
        return dest_path
    if data_url.endswith('.zip'):
        dest_path += '.zip'
    download(data_url, dest_path)
    if data_url.endswith('.zip'):
        with zipfile.ZipFile(dest_path, 'r') as archive:
            archive.extractall(root_path)
        remove_file(dest_path)
        dest_path = dest_path[:-len('.zip')]
    return dest_path


def load_dictionary(mini=False):
    dic_path = HanLP.Config.CoreDictionaryPath
    # dic_path = os.path.abspath(os.path.join(os.pardir, 'data/dictionary/CoreNatureDictionary.txt'))
    mini_dic_path = dic_path.replace('.txt', '.mini.txt')
    dic = IOUtil.loadDictionary([mini_dic_path if mini else dic_path])
    return set(dic.keySet())


def my_cws_corpus():
    data_root = test_data_path()
    corpus_path = os.path.join(data_root, 'my_cws_corpus.txt')
    if not os.path.isfile(corpus_path):
        with open(corpus_path, 'w', encoding='utf-8') as out:
            out.write('''商品 和 服务
商品 和服 物美价廉
服务 和 货币''')
    return corpus_path


def get_eval_result(result):
    return 'P:{:.2f} R:{:.2f} F1:{:.2f} OOV-R:{:.2f} IV-R:{:.2f}'.format(result.P, result.R, result.F1, result.OOV_R, result.IV_R)

