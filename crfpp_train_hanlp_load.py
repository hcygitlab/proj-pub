from pyhanlp import JClass
from pyhanlp.static import HANLP_JAR_PATH
from test_utility import test_data_path, my_cws_corpus
import os


CRFSegmenter = JClass('com.hankcs.hanlp.model.crf.CRFSegmenter')


TXT_CORPUS_PATH = my_cws_corpus()
TSV_CORPUS_PATH = TXT_CORPUS_PATH + ".tsv"
TEMPLATE_PATH = test_data_path() + "/cws-template.txt"
CRF_MODEL_PATH = test_data_path() + "/crf-cws-model"
CRF_MODEL_TXT_PATH = test_data_path() + "/crf-cws-model.txt"


def train_or_load(corpus_txt_path=TXT_CORPUS_PATH, model_txt_path=CRF_MODEL_TXT_PATH):
    if os.path.isfile(model_txt_path):  # 已訓練，直接載入
        segmenter = CRFSegmenter(model_txt_path)
        return segmenter
    else:
        segmenter = CRFSegmenter()  # 創建空白分詞器
        segmenter.convertCorpus(corpus_txt_path, TSV_CORPUS_PATH)  # 執行轉換
        segmenter.dumpTemplate(TEMPLATE_PATH)  # 導出特徵模板
        # 交给CRF++训练
        print("語料已轉換為 %s ，特徵模板已導出為 %s" % (TSV_CORPUS_PATH, TEMPLATE_PATH))
        print("請安裝CRF++後執行 crf_learn -f 3 -c 4.0 %s %s %s -t" % (TEMPLATE_PATH, TSV_CORPUS_PATH, CRF_MODEL_PATH))
        print("或者執行移植版 java -cp %s com.hankcs.hanlp.model.crf.crfpp.crf_learn -f 3 -c 4.0 %s %s %s -t" % (
            HANLP_JAR_PATH, TEMPLATE_PATH, TSV_CORPUS_PATH, CRF_MODEL_PATH))


if __name__ == '__main__':
    segment = train_or_load()
    if segment:
        print(segment.segment("商品和服务"))
