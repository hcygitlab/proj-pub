from pyhanlp import JClass
from test_utility import ensure_data

IClassifier = JClass('com.hankcs.hanlp.classification.classifiers.IClassifier')
NaiveBayesClassifier = JClass('com.hankcs.hanlp.classification.classifiers.NaiveBayesClassifier')
LinearSVMClassifier = JClass('com.hankcs.hanlp.classification.classifiers.LinearSVMClassifier')
chn_senti_corp = ensure_data('ChnSentiCorp', 'http://file.hankcs.com/corpus/ChnSentiCorp.zip')


def predict(classifier, text):
    return text, classifier.classify(text)


if __name__ == '__main__':
    # classifier = NaiveBayesClassifier()
    classifier = LinearSVMClassifier()
    classifier.train(chn_senti_corp) # 訓練後模型可持久化，下次不必訓練
    # predict(classifier, '前台客房服务态度非常好！早餐很丰富，房价很干净。再接再厉！')
    # predict(classifier, '结果大失所望，灯光昏暗，空间极其狭小，床垫质量恶劣，房间还伴着一股霉味。')
    # predict(classifier, '可利用文本分类实现情感分析，效果不是不行')
    # predict(classifier, '一直都经期所以臭脸')
    senti_classifier = classifier
