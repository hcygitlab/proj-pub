from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import requests
import re
import json
import time
from urllib.parse import urljoin, urlparse
import psycopg2 as pg
import pandas.io.sql as psql
from psycopg2.extras import execute_values


def connect():
    connection_string = 'dbname=nlp user=yuan host=127.0.0.1 port=5432'
    return pg.connect(connection_string)


def request_content(url):
    base_url = 'https://www.ptt.cc'
    headers = {'User-Agent': 'Mozilla/5.0'}
    cookies = {'over18': '1'}
    response = None
    try:
        response = requests.get(urljoin(base_url, url), headers=headers, cookies=cookies)
        if response.status_code != requests.codes.ok:
            raise Exception('request failed')
        response.encoding = 'utf-8'
    except Exception as err:
        print(err)
    else:
        # response = BeautifulSoup(response.text, 'lxml')
        response = response.text
    finally:
        return response


def request(url):
    base_url = 'https://www.ptt.cc'
    headers = {'User-Agent': 'Mozilla/5.0'}
    cookies = {'over18': '1'}
    response = None
    try:
        response = requests.get(urljoin(base_url, url), headers=headers, cookies=cookies)
        if response.status_code != requests.codes.ok:
            raise Exception('request failed')
        response.encoding = 'utf-8'
    except Exception as err:
        print(err)
    else:
        response = BeautifulSoup(response.text, 'lxml')
    finally:
        return response


def get_pagination(obj):
    paging = obj.find('div', 'btn-group-paging')
    pages = paging.find
    #     pages = paging.find_all('a', href=True)
    prev_page = paging.find('a', string='‹ 上頁')
    next_page = paging.find('a', string='下頁 ›')
    return {
        'prev': prev_page['href'] if prev_page.has_attr('href') else None,
        'next': next_page['href'] if next_page.has_attr('href') else None
    }


def get_links(obj):
    links = obj.select('.r-ent > .title > a')
    return [{'link': link['href'], 'title': link.text.replace("'", '')} for link in links] if len(links) > 0 else None


def get_page(obj):
    pagination = get_pagination(obj)
    return {
        'prev': pagination['prev'],
        'next': pagination['next'],
        'items': get_links(obj)
    }


def get_all_links():
    end_page = '/bbs/Gossiping/index24540.html'
    request_page = '/bbs/Gossiping/index763.html'
    while True:
        #         print(f'request_page: {request_page}')
        #         print(f'end_page: {end_page}')
        if request_page == end_page:
            break
        items = []
        l = request(request_page)
        ls = get_page(l)
        if ls['next'] is None:
            break
        request_page = ls['next']
        items.extend(ls['items'])

        conn = connect()
        try:
            with conn.cursor() as curs:
                args_str = ','.join(["('" + item['link'] + "','" + item['title'] + "')" for item in items])
                curs.execute('insert into records (link,title) values %s' % args_str)
                conn.commit()
        except Exception as e:
            conn.rollback()
            print(e)
            raise e
        finally:
            if conn is not None:
                conn.close()


def fetch_links_pd():
    conn = connect()
    try:
        return psql.read_sql('select link,title from records;', conn)
    except Exception as err:
        print(err)
        raise err
    finally:
        if conn is not None:
            conn.close()


def fetch_links(start_lid):
    conn = connect()
    try:
        with conn.cursor() as curs:
            curs.execute(f'select link,title from records where lid > {start_lid};')
            rtn_links = curs.fetchall()
            return rtn_links
    except Exception as e:
        print(e)
        raise e


def get_comments(obj):
    """
    :param obj:
    :return: [{ tag, userid, comment, datetime }, ...]
    """
    comments = []
    push_list = obj.select('.push')
    if len(push_list) > 1:
        for push_data in push_list:
            if push_data.find('span', 'push-tag') is None:
                print('no push-tag')
            data = {
                'tag': push_data.find('span', 'push-tag').text.strip(),
                'userid': push_data.find('span', 'push-userid').text,
                'comment': push_data.find('span', 'push-content').text.strip(':').strip(),
                'datetime': push_data.find('span', 'push-ipdatetime').text.strip()
            }
            comments.append(data)
    return comments


def date_converter(date_string):
    """
    eg: 'Wed Jun 22 02:53:45 2005' => '2005-06-22 02:53:45'
    """
    month_dict = {
        'Jan': '01',
        'Feb': '02',
        'Mar': '03',
        'Apr': '04',
        'May': '05',
        'Jun': '06',
        'Jul': '07',
        'Aug': '08',
        'Sep': '09',
        'Oct': '10',
        'Nov': '11',
        'Dec': '12'
    }
    dd = [d.zfill(2) for d in date_string.split(' ') if len(d) > 0]
    data = {
        'year': dd[4],
        'month': month_dict[dd[1]],
        'day': dd[2],
        'time': dd[3],
        'string': f'{dd[4]}-{month_dict[dd[1]]}-{dd[2]} {dd[3]}'
    }
    return pd.Series(data)


def get_article(obj):
    start = obj.select('.article-metaline')[-1]
    end = obj.find('span', text=re.compile(r'^※ 發信站: 批踢踢實業坊'), attrs={'class': 'f2'})

    rows = []
    for element in start.next_siblings:
        try:
            rows.append(element.text.strip())
        except:
            rows.append(str(element).strip('').replace('\n', '').replace('\u3000', '').replace(' ', '').rstrip('--'))
        if element is end:
            break
    if len(rows) == 0:
        return ''

    result = []
    for row in rows[:-1]:
        if len(row) > 0 and not row.startswith('http') and not row.startswith('※') and not row.startswith(':'):
            result.append(row)
    return ''.join(result)


def get_content(link):
    obj = request(link)
    metalines = obj.select('#main-content')[0].select('.article-metaline .article-meta-value')
    author = metalines[0].text.split(' ')[0]
    metaline_title = metalines[1].text
    post_time = date_converter(metalines[2].text).string
    article = get_article(obj)
    comments = get_comments(obj)
    link_arr = link.split('/')

    return {
        'alias': link_arr[5].rstrip('.html') if link.startswith('https') else link_arr[3].rstrip('.html'),
        'author': author,
        'title': metaline_title,
        'post_time': post_time,
        'article': article,
        'comments': comments
    }


def write_log(message):
    with open('./fails.log', 'a') as file_obj:
        file_obj.write(f'{message}/n')


def save_result(obj):
    conn = connect()

    insert_article_sql = 'insert into articles (bid,author,title,article,alias,post_time) \
    values (%s,%s,%s,%s,%s,%s) returning aid;'
    insert_article_data = (
        1,
        obj['author'],
        obj['title'],
        obj['article'],
        obj['alias'],
        obj['post_time'],
    )

    #     insert_comments_sql = 'insert into comments (aid,bid,userid,tag,comment,datetime) \
    #     values (%s,%s,%s,%s,%s,%s);'
    insert_comments_sql = 'insert into comments (aid,bid,userid,tag,comment,datetime) values %s;'
    insert_comments_data = []
    try:
        with conn.cursor() as curs:
            curs.execute('select aid from articles where alias=%s;', (obj['alias'],))
            existing_article = curs.fetchone()
            if existing_article is not None:
                print(f"article {obj['title']} exists")
                return
            curs.execute(insert_article_sql, insert_article_data)
            rtn = curs.fetchone()
            #             conn.commit()
            rtn_aid = rtn[0]

            if len(obj['comments']) == 0:
                return

            for comment in obj['comments']:
                curs.execute('select cid from comments where aid=%s and comment=%s;',
                             (rtn_aid, comment['comment'],))
                existing_comment = curs.fetchone()
                if existing_comment is not None:
                    print(f"comment {comment['comment']} exists")
                    continue
                insert_comments_data.append(
                    (
                        rtn_aid,
                        1,
                        comment['userid'],
                        comment['tag'],
                        comment['comment'],
                        comment['datetime'],
                    )
                )
            #             curs.execute(insert_comments_sql, insert_comments_data)
            execute_values(curs, insert_comments_sql, insert_comments_data)

            conn.commit()
    except Exception as e:
        conn.rollback()
        write_log(e)
        raise e
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    links = fetch_links(45065)
    print('=== start ===')
    for link, title in links:
        try:
            contents = get_content(link)
            save_result(contents)
        except Exception as e:
            print(link)
            print(e)
    print('=== finish ===')
    # content = get_content('/bbs/Gossiping/M.1557414270.A.AD9.html')
    # print(content)
