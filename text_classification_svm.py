from pyhanlp.static import STATIC_ROOT, download
from pyhanlp import SafeJClass, HanLP
import os
from text_classification_nb import sogou_corpus_path
import pandas as pd


def install_jar(name, url):
    dst = os.path.join(STATIC_ROOT, name)
    if os.path.isfile(dst):
        return dst
    download(url, dst)
    return dst


install_jar('text-classification-svm-1.0.2.jar', 'http://file.hankcs.com/bin/text-classification-svm-1.0.2.jar')
install_jar('liblinear-1.95.jar', 'http://file.hankcs.com/bin/liblinear-1.95.jar')

LinearSVMClassifier = SafeJClass('com.hankcs.hanlp.classification.classifiers.LinearSVMClassifier')
IOUtil = SafeJClass('com.hankcs.hanlp.corpus.io.IOUtil')


def train_or_load_classifier():
    model_path = sogou_corpus_path + '.svm.ser'
    if os.path.isfile(model_path):
        return LinearSVMClassifier(IOUtil.readObjectFrom(model_path))
    classifier = LinearSVMClassifier()
    classifier.train(sogou_corpus_path)
    model = classifier.getModel()
    IOUtil.saveObjectTo(model, model_path)
    return LinearSVMClassifier(model)


def predict(classifier, text):
    return [HanLP.s2tw(text), HanLP.s2tw(classifier.classify(text))]


if __name__ == '__main__':
    classifier = train_or_load_classifier()
    sentences = [
        '潜艇具有很强的战略威慑能力与实战能力',
        '研究生考录模式亟待进一步专业化',
        '如果真想用食物解压,建议可以食用燕麦',
        '通用及其部分竞争对手目前正在考虑解决库存问题'
    ]
    text_classification_svm_result = []
    for sentence in sentences:
        result = predict(classifier, sentence)
        text_classification_svm_result.append(result)
    text_classification_svm_result = pd.DataFrame(data=text_classification_svm_result, columns=['文章', '分類'])
