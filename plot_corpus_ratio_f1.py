from tempfile import NamedTemporaryFile
import numpy as np


from msr import msr_train, msr_model, msr_gold, msr_dict, msr_output, msr_test
import matplotlib.pyplot as plt
from pyhanlp import SafeJClass, JClass, PerceptronLexicalAnalyzer

plt.rcParams['font.sans-serif'] = ['Heiti TC']
plt.rcParams['axes.unicode_minus'] = False

CWSEvaluator = SafeJClass('com.hankcs.hanlp.seg.common.CWSEvaluator')
CWSTrainer = JClass('com.hankcs.hanlp.model.perceptron.CWSTrainer')


def train_evaluate(ratio):
    partial_corpus = NamedTemporaryFile(delete=False).name
    with open(msr_train, encoding='utf-8') as src, open(partial_corpus, 'w', encoding='utf-8') as dst:
        all_lines = src.readlines()
        dst.writelines(all_lines[:int(ratio * len(all_lines))])

    model = CWSTrainer().train(partial_corpus, partial_corpus, msr_model, 0, 50, 8).getModel()  # train
    result = CWSEvaluator.evaluate(PerceptronLexicalAnalyzer(model).enableCustomDictionary(False),
                                   msr_test, msr_output, msr_gold, msr_dict)
    # return result.F1
    return float(str(result).split()[2][3:])


if __name__ == '__main__':
    x = [r / 10 for r in range(1, 11)]
    y = [train_evaluate(r) for r in x]
    plt.title("語料庫規模對準確率的影響")
    plt.xlabel("語料庫規模（萬字符）")
    plt.ylabel("準確率")
    plt.xticks([int(r / 10 * 405) for r in range(1, 11)])
    plt.yticks(np.arange(91, 97.5, 0.5))
    plt.plot([int(r / 10 * 405) for r in range(1, 11)], y, color='b')
    plt.grid()
    plt.show()
