from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import requests
import re
import json
import time
from urllib.parse import urljoin, urlparse
import psycopg2 as pg
import pandas.io.sql as psql
from psycopg2.extras import execute_values


def connect():
    connect_string = 'dbname=nlp user=yuan host=127.0.0.1 port=5432'
    return pg.connect(connect_string)


def request_content(url):
    base_url = 'https://www.ptt.cc'
    headers = {'User-Agent': 'Mozilla/5.0'}
    cookies = {'over18': '1'}
    response = None
    try:
        response = requests.get(urljoin(base_url, url), headers=headers, cookies=cookies)
        if response.status_code != requests.codes.ok:
            raise Exception('request failed')
        response.encoding = 'utf-8'
    except Exception as err:
        print(err)
    else:
        response = BeautifulSoup(response.text, 'lxml').find(id='main-content').text
    finally:
        return response


def get_records(start_lid):
    conn = connect()
    try:
        with conn.cursor() as curs:
            curs.execute(f'select lid,link,title,content from records where lid > {start_lid};')
            return curs.fetchall()
    except Exception as e:
        print(e)
        raise e
    finally:
        if conn is not None:
            conn.close()


def get_article_text(link):
    rtn = request_content(link)
    print(type(rtn))
    if rtn is not None:
        return rtn


def save_link_content(lid, content):
    conn = connect()
    try:
        with conn.cursor() as curs:
            curs.execute('update test set text=%s where aid=%s', (content, lid,))
            conn.commit()
    except Exception as e:
        conn.rollback()
        print(e)
        raise e
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    records = get_records(0)
    for lid, link, title, content in records[10000:10010]:
        print(link)
        if content is not None:
            continue
        rtn_content = request_content(link)
        print(type(rtn_content))
        # if len(rtn_content) > 0:
        #     save_link_content(lid, rtn_content)
