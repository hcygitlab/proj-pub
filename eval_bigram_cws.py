from pyhanlp import SafeJClass
from msr import msr_dict, msr_train, msr_model, msr_test, msr_output, msr_gold
from ngram_segment import train_bigram, load_bigram
import sys

if __name__ == '__main__':
    segment = load_bigram(msr_model, verbose=False)  # load
    CWSEvaluator = SafeJClass('com.hankcs.hanlp.seg.common.CWSEvaluator')
    result = CWSEvaluator.evaluate(segment, msr_test, msr_output, msr_gold, msr_dict)
    eval_bigram_cws_result = 'P:{:.2f} R:{:.2f} F1:{:.2f} OOV-R:{:.2f} IV-R:{:.2f}'.format(result.P, result.R, result.F1, result.OOV_R, result.IV_R)
