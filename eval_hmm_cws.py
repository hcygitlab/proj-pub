from pyhanlp import JClass, SafeJClass
from msr import msr_dict, msr_train, msr_model, msr_test, msr_output, msr_gold
from test_utility import get_eval_result

FirstOrderHiddenMarkovModel = JClass('com.hankcs.hanlp.model.hmm.FirstOrderHiddenMarkovModel')
SecondOrderHiddenMarkovModel = JClass('com.hankcs.hanlp.model.hmm.SecondOrderHiddenMarkovModel')
HMMSegmenter = JClass('com.hankcs.hanlp.model.hmm.HMMSegmenter')
CWSEvaluator = SafeJClass('com.hankcs.hanlp.seg.common.CWSEvaluator')


def train(corpus, model):
    segmenter = HMMSegmenter(model)
    segmenter.train(corpus)
    # print(segmenter.segment('商品和服务'))
    return segmenter.toSegment()


def evaluate(segment):
    CWSEvaluator = SafeJClass('com.hankcs.hanlp.seg.common.CWSEvaluator')
    result = CWSEvaluator.evaluate(segment, msr_test, msr_output, msr_gold, msr_dict)
    return get_eval_result(result)


if __name__ == '__main__':
    segment = train(msr_train, FirstOrderHiddenMarkovModel())
    first_order_hmm_result = evaluate(segment)
    segment = train(msr_train, SecondOrderHiddenMarkovModel())
    second_order_hmm_result = evaluate(segment)
