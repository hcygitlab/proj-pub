import psycopg2 as pg
# import pandas.io.sql as psql
from psycopg2.extras import execute_values, RealDictCursor, DictCursor, NamedTupleCursor
import re
import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plotly.express as px
from urllib.parse import urljoin #, urlparse
import json
import asyncpg
from time import process_time
import os

con_param = { 'user': 'yuan', 'database': 'nlp' }

# plt.rcParams['figure.figsize'] = 20, 10
# plt.rcParams['figure.dpi'] = 100
# plt.rcParams['font.family'] = 'Heiti TC'

figure = {
    'figsize': (20, 10),
    'dpi': 144
}
font = {
    'family' : 'Heiti TC',
    'weight' : 'normal',
    'size'   : 16
}
plt.rc('figure', **figure)
plt.rc('font', **font)


def write_log(message):
    with open('./fails.log', 'a') as file_obj:
        file_obj.write(f'{message}\n')


def request(url, get_content=False):
    base_url = 'https://www.ptt.cc'
    headers = {'User-Agent': 'Mozilla/5.0'}
    cookies = {'over18': '1'}
    response = None
    try:
        response = requests.get(urljoin(base_url, url), headers=headers, cookies=cookies)
        if (response.status_code != 200):
            raise Exception(f'{url}request failed')
        response.encoding = 'utf-8'
    except Exception as err:
        write_log(url)
        print(err)
    else:
        response = BeautifulSoup(response.text, 'lxml').find(id='main-content').get_text() if get_content else BeautifulSoup(response.text, 'lxml')
    finally:
        return response


def is_url(url):
    regex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return re.match(regex, url) is not None


def connect():
    return pg.connect('dbname=nlp user=yuan host=127.0.0.1 port=5432')


class Connect:
    def __init__(self, connection_string='dbname=nlp user=yuan host=127.0.0.1 port=5432'):
        self.connection_string = connection_string
        self.conn = None

    def __enter__(self):
        if self.conn is not None:
            raise RuntimeError('already connected')
        self.conn = pg.connect(self.connection_string)
        return self.conn

    def __exit__(self, exc_ty, exc_val, tb):
        if exc_ty is not None:
            self.conn.rollback()
        self.conn.close()


class DB:
    def __init__(self, **settings):
        self.settings = settings
        self._pool = None

    @property
    async def pool(self):
        if self._pool is None:
            self._pool: asyncpg.pool.Pool = await asyncpg.create_pool(**self.settings)
        return self._pool

    # async def fetchrow(self, *args, **kwargs):
    #     return (await (await self.pool).fetchrow(*args, **kwargs))

    async def fetch(self, *args, **kwargs):
        return (await (await self.pool).fetch(*args, **kwargs))


def init_db() -> DB:
    return DB(
        database='nlp',
        user='yuan',
    )


class Parser:
    def __init__(self, text, id):
        self.text = text
        self.lines = []
        self.author = ''
        self.title = ''
        self.post_time = ''
        self.article = ''
        self.comments = []


def parse_datetime(date_string):
    """
    convert date_string
    :param date_string: Wed Jun 22 02:53:45 2005
    :return: 2005-06-22 02:53:45
    """
    month_dict = {
        'Jan': '01',
        'Feb': '02',
        'Mar': '03',
        'Apr': '04',
        'May': '05',
        'Jun': '06',
        'Jul': '07',
        'Aug': '08',
        'Sep': '09',
        'Oct': '10',
        'Nov': '11',
        'Dec': '12'
    }
    dd = [d.zfill(2) for d in date_string[2:].split(' ') if len(d) > 0]
    data = {
        'year': dd[4],
        'month': month_dict[dd[1]],
        'day': dd[2],
        'time': dd[3],
        'string': f'{dd[4]}-{month_dict[dd[1]]}-{dd[2]} {dd[3]}'
    }
    return data['string']


def parse_meta(s):
    if not s.startswith('作者'):
        raise ValueError('找不到作者')
    date_string = re.search('時間([SMTWF].+)', s).group(1)[:24]
    post_time = parse_datetime(date_string)
    author_name = None
    author = re.search('作者(.*)看板' if '看板' in s else '作者(.*)標題', s).group(1).rstrip()
    if '(' in author:
        author_name = author.split('(')[0].rstrip()
    else:
        author_name = author
    title = re.search('標題(.*)', s.replace(date_string, '')).group(1).rstrip('時間')
    result = {
        'author': author_name,
        'post_time': post_time,
        'title': title
    }
    return result


def remove_ip(string):
    regex = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
    return regex.sub('', string)


def parse_comments(lines):
    result = []

    for line in lines:
        tag = line[0:1]
        tag = tag if tag in ['推', '噓', '→'] else None
        if tag is None:
            continue
        datetime = line[-11:]
        arr = line[2:].split(':', 1)
        if len(arr) < 2:
            continue
        userid = arr[0]
        comment = remove_ip(arr[1]).replace(datetime, '').strip()
        result.append({
            'tag': tag,
            'userid': userid,
            'comment': comment,
            'datetime': datetime
        })

    return result if len(result) > 0 else None


def remove_signature(text):
    retext = re.sub(r'-{3,}', '', text)
    cleaner = re.compile(r'--.*?--', re.DOTALL)
    # cleaner.findall(text)
    return re.sub(cleaner, '', retext)


def parse_article(text):
    lines = remove_signature(text).splitlines()
    meta = None
    try:
        meta = parse_meta(lines[0])
    except:
        return

    body = lines[1:]
    article_end_index = None
    push_index = None
    article_arr = []
    for index, line in enumerate(body):
        if len(line) == 0 or line.endswith('之銘言：') or line.startswith(': ') or line.startswith('Sent from') or line.rstrip() == '--':
            continue
        if line.startswith('※ 發信站') or ('※ 發信站' not in text and line.startswith('※ 編輯')):
            article_end_index = index
        if line.startswith('※ 文章網址') or ('※ 文章網址' not in text and line.startswith('※ 編輯')):
            push_index = index + 1
            break
        if article_end_index is None:
            article_arr.append(line)

    article = ''.join(article_arr)
    comments = parse_comments(body[push_index:])

    result = {
        'author': meta['author'],
        'title': meta['title'],
        'post_time': meta['post_time'],
        'article': article,
        'comments': comments
    }
    return result


async def retrieve(db: DB):
    data = []
    print('start retrieve process')
    count = 0
    c_count = 0
    start = process_time()
    records = await db.fetch("select lid,content from records where content ~* '[推噓→]'")
    print(f'took {process_time() - start}s, got {len(records)} records')
    start = process_time()
    for record in records:
        try:
            result = parse_article(record['content'])
            if result is not None:
                count += 1
                if result['comments'] is not None and len(result['comments']) > 0:
                    c_count += 1
                    data.append(result)
                # else:
                #     print(f"{record['lid']} has no comment")

        except Exception as e:
            print(f"{record['lid']}: {e}")
    print(f'took {process_time() - start}s to parse')
    print(f'valid count: {count}')
    print(f'has comments count: {c_count}')

    return data



def save_results(rows):
    print(f'start saving {len(rows)} results')
    start = process_time()

    insert_article_stmt = \
    'insert into articles(bid,author,title,article,category,post_time) values (%s,%s,%s,%s,%s,%s) returning aid;'

    insert_comments_sql = 'insert into comments(aid,bid,userid,tag,comment,datetime) values %s;'

    conn = connect()

    try:
        with conn.cursor() as cur:
            for row in rows:
                if not row['post_time'].startswith('2019'):
                    continue
                cs = row['comments']
                insert_article_data = (1,row['author'],row['title'],row['article'],0,row['post_time'],)
                cur.execute(insert_article_stmt,insert_article_data)
                aid = cur.fetchone()[0]

                if len(cs) > 0:
                    insert_comments_data = [(aid,1,c['userid'],c['tag'],c['comment'],c['datetime']) for c in cs]
                    execute_values(cur, insert_comments_sql, insert_comments_data)

            conn.commit()
    except Exception as e:
        conn.rollback()
        raise e
    finally:
        if conn is not None:
            conn.close()

    print(f'took {process_time() - start}s to save')


def sum_push_by_aids(aids, comments):
    return [comments[comments.aid==aid].tag.sum() for aid in aids]

def retrieve_all():
    con = connect()
    article_rows = None
    comment_rows = None

    push_mapper = {
        '推': 1,
        '噓': -1,
        '→': 0
    }

    try:
        article_rows = pd.read_sql_query(
            'select aid,author,title,article,post_time from articles order by post_time',
            con,
            index_col='aid'
            )
        comment_rows = pd.read_sql_query(
            'select cid,aid,userid,tag,comment,datetime from comments',
            con,
            index_col='cid'
            )
    except Exception as e:
        raise e
    else:
        article_rows['date'] = article_rows['post_time'].dt.strftime('%Y-%m-%d')
        # 清除標題 [公告] 開頭 且 包含'水桶'/'置底閒聊'/'清除異常帳號'
        article_rows = article_rows[~article_rows.title.apply(
            lambda t: (t.startswith('[公告]') or t.startswith('Fw: [公告]')) and ('桶' in t or '置底閒聊' in t or '清除異常帳號' in t))]
        # 2019-04-29以前的資料有遺漏
        article_rows = article_rows[article_rows.date > '2019-04-28']
        comment_rows = comment_rows[comment_rows.aid.apply(lambda c: c in article_rows.index)]
        # 過濾內容為連結的評論
        comment_rows = comment_rows[~comment_rows.comment.apply(lambda c: c.startswith('http'))]
        comment_rows['tag'] = comment_rows.tag.map(push_mapper)
        # 計算文章推噓數 (耗時!)
        # use df.to_numpy() or df.array instead of df.values
        article_rows['push'] = sum_push_by_aids(article_rows.index.to_numpy(), comment_rows)
        print(f'articles:\t{len(article_rows)}')
        print(f'comments:\t{len(comment_rows)}')
        return article_rows, comment_rows
    finally:
        if con is not None:
            con.close()


def retrieve_parquets():
    data_folder = 'data/ptt/gossiping'
    return pd.read_parquet(os.path.join(data_folder, 'articles.parquet')), pd.read_parquet(os.path.join(data_folder, 'comments.parquet'))


def draw_daily_count(articles):
    if articles is None:
        print('請傳入文章資料')
    groupby_date = articles.groupby('date')
    most_count, least_count = groupby_date.count()['article'].max(), groupby_date.count()['article'].min()
    def get_date_by_count(count):
        return groupby_date.count()[groupby_date.count()['article'] == count].index[0]
    date_most, date_least = get_date_by_count(most_count), get_date_by_count(least_count)
    title = f'文章篇數: {len(articles)}, 單日最多: {most_count} ({get_date_by_count(most_count)}), 單日最少: {least_count} ({get_date_by_count(least_count)})'
    ax = groupby_date.count()['article'].plot(title=title,figsize=(20,4), legend=True)
    # ax.set_xlabel('日期')

    fig = px.line(groupby_date.count()['article'], title=title)
    fig.show()


def get_antusd():
    '''
    台大增廣意見詞詞典 ANTUSD
    '''
    names = ['word','score','pos','neu','neg','1','2']
    usecols = ['word','score','pos','neu','neg']
    return pd.read_csv('data/dictionary/ANTUSD/opinion_word_utf8.csv', names=names, usecols=usecols)


antusd = get_antusd()
antusd_neg = antusd[(antusd.neg > 0) & (antusd.neu == 0) & (antusd.pos == 0) & (antusd.score < 0)]
antusd_pos = antusd[(antusd.pos > 0) & (antusd.neu == 0) & (antusd.neg == 0) & (antusd.score > 0)]
antusd_neu = antusd[(antusd.neg == 0) & (antusd.neu > 0) & (antusd.pos == 0) & (antusd.score == 0)]
antusd_neg_list = list(antusd_neg.word)
antusd_pos_list = list(antusd_pos.word)
antusd_neu_list = list(antusd_neu.word)


def calc_score(text):
    pos_arr = [1 if text.find(i) != -1 else 0 for i in antusd_pos_list]
    neu_arr = [1 if text.find(i) != -1 else 0 for i in antusd_neu_list]
    neg_arr = [1 if text.find(i) != -1 else 0 for i in antusd_neg_list]
    sum_tuple = sum(pos_arr), sum(neu_arr), sum(neg_arr)
    index = sum_tuple.index(max(sum_tuple)) if any(sum_tuple) else -1
    return 1 if index == 0 else -1 if index == 2 else 0


def group_comments_by_aid_userid(comments, max_count=None):
    '''
    groupby 'comment'
    '''
    count = 0
    result = []
    for a in comments[~comments.comment.apply(lambda x: 'http' in x and len(x) > 0)].groupby(['aid']):
        if max_count is not None and count == max_count:
            break
        for c in a[1].groupby(['userid']):
            result.append([a[0], c[0], ''.join(c[1].comment)])
        if max_count is not None:
            count += 1
    return pd.DataFrame(columns=['aid', 'userid', 'comment'], data=result)


def retrieve_grouped_comments():
    return pd.read_parquet(os.path.join('data/ptt/gossiping', 'comments_grouped.parquet'))


def retrieve_comments_senti():
    return pd.read_parquet(os.path.join('data/ptt/gossiping', 'comments_with_score.parquet'))
