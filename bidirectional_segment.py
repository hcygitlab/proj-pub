from test_utility import load_dictionary, s2tw, tw2s
import numpy as np
import pandas as pd

def forward_segment(text):
    dic = load_dictionary()
    word_list = []
    i = 0
    while i < len(text):
        longest_word = text[i]
        for j in range(i + 1, len(text) + 1):
            word = text[i:j]
            if word in dic:
                if len(word) > len(longest_word):
                    longest_word = word
        word_list.append(longest_word)
        i += len(longest_word)
    return ','.join(word_list)


def backward_segment(text):
    dic = load_dictionary()
    word_list = []
    i = len(text) - 1
    while i >= 0:
        longest_word = text[i]
        for j in range(0, i):
            word = text[j: i + 1]
            if word in dic:
                if len(word) > len(longest_word):
                    longest_word = word
                    break
        word_list.insert(0, longest_word)
        i -= len(longest_word)
    return ','.join(word_list)


def bidirectional_segment(text):
    def count_single_char(word_list: list):
        return sum(1 for word in word_list if len(word) == 1)
    f = forward_segment(text)
    b = backward_segment(text)
    result = None
    if len(f) < len(b):
        result = f
    elif len(f) > len(b):
        result = b
    else:
        if count_single_char(f) < count_single_char(b):
            result = f
        else:
            result = b
    return result


if __name__ == '__main__':
    forward_backward_bidirectional_examples = [
        tw2s('研究生命起源'),
    ]

    columns = ['正向最長匹配:', '逆向最長匹配:', '雙向最長匹配:']

    results = []
    for text in forward_backward_bidirectional_examples:
        data = [forward_segment(text), backward_segment(text), bidirectional_segment(text)]
        results.append(data)
    output = pd.DataFrame(columns=columns, data=np.array(results))
    bidirectional_segment_result = s2tw(output.to_string())
