import re
from collections import Counter
import os
from test_utility import ensure_data
import pandas as pd


def count_corpus(train_path: str, test_path: str):
    train_counter, train_freq, train_chars = count_word_freq(train_path)
    test_counter, test_freq, test_chars = count_word_freq(test_path)
    test_oov = sum(test_counter[w] for w in (test_counter.keys() - train_counter.keys()))
    return train_chars / 10000, len(
        train_counter) / 10000, train_freq / 10000, train_chars / train_freq, test_chars / 10000, len(
        test_counter) / 10000, test_freq / 10000, test_chars / test_freq, test_oov / test_freq * 100


def count_word_freq(train_path):
    f = Counter()
    with open(train_path, encoding='utf-8') as src:
        for line in src:
            for word in re.compile('\\s+').split(line.strip()):
                f[word] += 1
    return f, sum(f.values()), sum(len(w) * f[w] for w in f.keys())


if __name__ == '__main__':
    sighan05 = ensure_data('icwb2-data', 'http://sighan.cs.uchicago.edu/bakeoff2005/data/icwb2-data.zip')

    train_columns = ['字符數(萬)', '詞語種數(萬)', '總詞頻(萬)', '平均詞長']
    test_columns = train_columns + ['OOV']
    index = ['pku', 'msr', 'as', 'cityu']

    train_rows = []
    test_rows= []


    for data in index:
        train_path = os.path.join(sighan05, 'training', '{}_training.utf8'.format(data))

        test_path = os.path.join(sighan05, 'gold',
                                 ('{}_testing_gold.utf8' if data == 'as' else '{}_test_gold.utf8').format(data))

        result = count_corpus(train_path, test_path)

        train_rows.append(['{:.0f}'.format(result[0]), '{:.0f}'.format(result[1]), '{:.0f}'.format(result[2]), '{:.1f}'.format(result[3])])
        test_rows.append(['{:.0f}'.format(result[4]), '{:.0f}'.format(result[5]), '{:.0f}'.format(result[6]), '{:.1f}'.format(result[7]), '{:.2f}'.format(result[8])])


    df_train = pd.DataFrame(index=index, columns=train_columns, data=train_rows)
    df_test = pd.DataFrame(index=index, columns=test_columns, data=test_rows)
    sighan05_statistics_result_train = df_train
    sighan05_statistics_result_test = df_test
